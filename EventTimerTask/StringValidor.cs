﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventTimerTask
{
    public class StringValidor : IStringValid
    {
        public string inputTime;
        public StringValidor(string inputTime)
        {
            this.inputTime = inputTime;
        }
        public void StrValidate(out DateTime dateTime)
        {
            if (!DateTime.TryParse(this.inputTime, out dateTime))
            {
                throw new
                    ArgumentException("This string format isn't correct");
            }
        }
    }
}
