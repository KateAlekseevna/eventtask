﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace EventTimerTask
{
   public interface ITimerWaitHandler
    {
        public AutoResetEvent timerWaitHandler { get; set; }

        public void Set()
        {
            timerWaitHandler.Set();
        }

        public void WaitOne()
        {
            timerWaitHandler.WaitOne();
        }
    }
}
