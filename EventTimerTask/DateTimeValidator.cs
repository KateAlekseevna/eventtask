﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventTimerTask
{
    public class DateTimeValidator : ITimeValidator
    {
        public void TimeValidate(DateTime inputUserTime)
        {
            if (inputUserTime < DateTime.Now)
            {
                throw new
                    ArgumentException("The input time is past");
            }
        }
    }
}
