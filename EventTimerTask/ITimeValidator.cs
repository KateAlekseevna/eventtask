﻿using System;

namespace EventTimerTask
{
    public interface ITimeValidator
    {
        void TimeValidate(DateTime inputUserTime);
    }
}