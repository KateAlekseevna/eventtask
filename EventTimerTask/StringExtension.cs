﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic.CompilerServices;

namespace EventTimerTask
{
    public static class StringExtension
    {
        public static string AddColonBetweenStr(this string str)
        {
            string outStr = String.Empty;

            string[] enteredDigit = str.Split(' ');

            for (int i = 0; i < enteredDigit.Length; i++)
            {
                outStr += enteredDigit[i] + ":";
            }
            return outStr.Remove(outStr.Length - 1, 1);
        }
    }
}
