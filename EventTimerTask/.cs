﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventTimerTask
{
    public class EnteredTimeExeptions : ArgumentException
    {
        public DateTime Value { get; }

        public EnteredTimeExeptions(string message, DateTime val)
            : base(message)
        {
            Value = val;
        }
    }

  

}
