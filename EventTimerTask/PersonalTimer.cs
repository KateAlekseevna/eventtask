﻿
namespace EventTimerTask
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    public class PersonalTimer
    {
        private ITimerWaitHandler _waitHandler;

        private ITimeValid _timeValid;

        private TimeSpan _interval;

        public delegate void PersonalTimerHandler();
        public event PersonalTimerHandler onTimerEvent;
        private double TimeElapsed => _timeValid.StopTime.Subtract(DateTime.Now).TotalSeconds;
        
        public bool Stopped
        {
            get => stopped;

            set => stopped = value;
        }

        private bool aborted;

        private bool stopped;

        /*
        DI by property
        public ITimeValidator TimeValidator
        {
            set => _timeValid = value;
        }

        public ITimerWaitHandler TimerWaitHandler
        {
            set => _waitHandler = value;
        }


        */
        public PersonalTimer(ITimeValid timeValid, ITimerWaitHandler waitHandler, TimeSpan interval)
        {
            _timeValid = timeValid;

            _interval = interval;

            _waitHandler = waitHandler;
        }

        public void Start(CancellationToken token)
        {
            Tick(token);

            Finish();
        }
        public async Task Tick(CancellationToken token)
        {

            while (TimeElapsed > 0)
            {
                if (token.IsCancellationRequested)
                {
                    Console.WriteLine("  Operation is aborted (Esc) ");
                    aborted = true;
                    break;
                }

                Helper.PrintInfo(TimeElapsed);

               await Task.Delay(_interval);
            }

            _waitHandler.Set();
        }
        public void Finish()
        {
            Console.WriteLine("Waiting for a timer to finish...");

            _waitHandler.WaitOne();

            stopped = true;

            if (!aborted)
            {
                try
                {
                    onTimerEvent?.Invoke();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            Console.WriteLine("Timer finished!");
        }
    }
}
