﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventTimerTask
{
    public interface IStrValidator
    {
        void StrValidate(string inputTime, out DateTime dateTime);
    }
}
