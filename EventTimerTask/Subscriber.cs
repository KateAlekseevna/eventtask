﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventTimerTask
{
    public static class Helper
    {
        public static void DisplayRedMessage()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("ALARM!!!");
            Console.ResetColor();
        }

        public static void GenerateExeptions()
        {
            throw new Exception("Alarm Exception is catched!");
        }

        public static void PrintInfo(double TimeElapsed)
        {
            Console.WriteLine($"Current: { DateTime.Now }. Alarm ring through {TimeElapsed} sec");
        }
    }
}
