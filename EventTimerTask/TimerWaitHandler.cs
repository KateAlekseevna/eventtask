﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace EventTimerTask
{
    public class TimerWaitHandler : ITimerWaitHandler
    {
        AutoResetEvent _waitHandler = new AutoResetEvent(false);

        public AutoResetEvent timerWaitHandler 
        {
            get
            {
                return _waitHandler;
            }
            set
            {
                _waitHandler = value;
            }
        }
        
    }
}
