﻿using System;

namespace EventTimerTask
{
    public interface ITimeValid
    {
        public DateTime StopTime { get; set; }
        void TimeValidate(DateTime inputUserTime);
    }
}