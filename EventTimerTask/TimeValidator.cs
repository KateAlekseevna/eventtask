﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventTimerTask
{
    public class TimeValidator : ITimeValid
    {
        public DateTime inputUserTime;
        public TimeValidator(DateTime inputUserTime)
        {
            this.inputUserTime = inputUserTime;
        }

        DateTime ITimeValid.StopTime { get => inputUserTime; set => inputUserTime= value; }

        public void TimeValidate(DateTime inputUserTime)
        {
            if (inputUserTime < DateTime.Now)
            {
                throw new
                    ArgumentException("The input time is past");
            }
        }
    }
}
