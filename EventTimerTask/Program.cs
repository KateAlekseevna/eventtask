﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using Serilog;
using Unity;
using Unity.Injection;
using Unity.Resolution;

namespace EventTimerTask
{
    public class Program
    {
        static void Main(string[] args)
        {
            CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
            CancellationToken token = cancelTokenSource.Token;

            ConsoleKeyInfo key;

            Console.WriteLine("Please, input the data through the separator 'space' in format - 'hour min sec'  ");
            Console.WriteLine("Press 'Esc' for stop process...");

            DateTime inputUserTime = new DateTime();
            
            var getUserInputStr = Console.ReadLine().AddColonBetweenStr();

            try
            {
                StringValidor strValid = new StringValidor(getUserInputStr);
                strValid.StrValidate(out inputUserTime);

                var unityContainer = new UnityContainer();
                unityContainer.RegisterType<PersonalTimer>();
                unityContainer.RegisterType<ITimeValid, TimeValidator>(new InjectionConstructor(inputUserTime));
                unityContainer.RegisterType<ITimerWaitHandler, TimerWaitHandler>();

                Console.WriteLine()::;
                /*TimeValidator timeValid = new TimeValidator(inputUserTime);
                TimerWaitHandler timerWaitHandler = new TimerWaitHandler();
                var myTimer = new PersonalTimer(timeValid, timerWaitHandler, TimeSpan.FromSeconds(1));
               */

                var myTimer = unityContainer.Resolve<PersonalTimer>(new ResolverOverride[]
                    {
                        new ParameterOverride("interval",TimeSpan.FromSeconds(1))
                    });

                myTimer.onTimerEvent += Helper.DisplayRedMessage;
                myTimer.onTimerEvent += Helper.GenerateExeptions;

                Task.Factory.StartNew(() => myTimer.Start(token));
               
                do
                {
                    key = Console.ReadKey();

                } while (key.Key != ConsoleKey.Escape && myTimer.Stopped);

                if (key.Key == ConsoleKey.Escape)
                {
                    cancelTokenSource.Cancel();
                }

                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

    }
}
