﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventTimerTask
{
    public class InputStrValidator : IStrValidator
    {
        public void StrValidate(string inputTime, out DateTime dateTime)
        {
            if (!DateTime.TryParse(inputTime, out dateTime))
            {
                throw new
                    ArgumentException("This string format isn't correct");
            }
        }

    }
}
