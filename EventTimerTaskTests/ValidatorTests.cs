﻿using System;
using EventTimerTask;
using Moq;
using NUnit.Framework;

namespace EventTimerTaskTests
{
    [TestFixture]
    public class ValidatorTests
    {

        DateTime dt;

        [SetUp]
        public void Setup()
        {

        }

        /// <summary>
        /// Performs necessary cleanup after every test
        /// </summary>
        [TearDown]
        public void Teardown()
        {

        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase("wrong time")]
        [TestCase("32:45:15")]
        public void StrValidate_InputStrIsNotValid_ExceptionThrown(string wrongInputStr)
        {
            // Act & Assert
            Assert.Throws<ArgumentException>(() => new InputStrValidator().StrValidate(wrongInputStr, out dt));
        }

        [TestCase("11:45:15")]
        [TestCase("00:00:00")]
        public void StrValidate_InputStrIsValid_NoExceptionThrown(string validInputStr)
        {
            // Act & Assert
            Assert.DoesNotThrow(() => new InputStrValidator().StrValidate(validInputStr, out dt));
        }

        [Test]
        public void TimeValidate_InputTimeIsPast_ExceptionThrown()
        {
            // Act & Assert
          //  Assert.Throws<ArgumentException>(() => new InputStrValidator().TimeValidate(DateTime.Now.AddHours(-2)));
        }

        [Test]
        public void TimeValidate_InputTimeIsValid_NoExceptionThrown()
        {
            // Act & Assert
           // Assert.DoesNotThrow(() => new InputStrValidator().TimeValidate(DateTime.Now.AddSeconds(15)));
        }
    }
}