﻿using EventTimerTask;
using NUnit.Framework;

namespace EventTimerTaskTests
{
    [TestFixture]
    public class StringExtensionTests
    {
        [Test]
        public void AddColonBetweenStr_Called_ExpectedResult()
        {
            // Arrange
            string inputStr = "13 45 34";
            // Act
            var result = inputStr.AddColonBetweenStr();
            //Assert
            Assert.AreEqual("13:45:34", result);
        }
    }
}